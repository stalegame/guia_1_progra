#Thomas Aráguiz - Progra_1

print("Para determinar el area se necesitara el radio y la hipotenusa")
hipotenusa = int(input("Por favor ingrese el tamaño de la hipotenusa: "))
radio = int(input("Por favor ingrese el tamaño del radio: "))

# se usa el teorema de pitagoras para conseguir el cateto faltante
cateto_faltante = (hipotenusa ** 2 - radio ** 2) ** 0.5

# se usan las formulas generales para sacar las areas
area_triangulo = radio * cateto_faltante
area_circunferencia = (3.14 * radio ** 2) / 2
area_total = area_triangulo + area_circunferencia

# Se entrega el resultado de la suma de las areas
print("El area de la figura es: ",area_total)
