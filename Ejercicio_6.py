#Thomas Aráguiz - Progra_1

# El while cumple la funcion de limitante
estacionamiento = 0
pago = 0
print("Al ingresar las hora de la forma H.MM => 1.30")
while estacionamiento < 5:

    estacionamiento = estacionamiento + 1
    tiempo = float(input("Ingrese las horas que estuvo estacionado: "))

    # se compara cuando el tiempo el tiempo no es entero sino decimal
    # Para mantener el entero y sumarle uno, para contar como la hora completa
    if tiempo != int(tiempo):
        tiempo = int(tiempo) + 1

    pago = tiempo * 100

    print("Lo que debe pagar es: ",pago)
    print("")
