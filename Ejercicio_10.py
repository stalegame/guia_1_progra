#Thomas Aráguiz - Progra_1

# se piden las medidas, pensando que el usario no pondra el lado A < C
lado_a = int(input("Ingrese la medida del lado a: "))
lado_b = int(input("Ingrese la medida del lado b: "))
lado_c = int(input("Ingrese la medida del lado c: "))

# se usan las formulas correspondientes para sacar las areas
area_rectangulo = lado_b * lado_c
area_triangulo = (lado_b * (lado_a - lado_c)) / 2
area_total = area_triangulo + area_rectangulo

print("El area de la figura es de: ",area_total)
