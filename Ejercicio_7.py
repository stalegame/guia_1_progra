#Thomas Aráguiz - Progra_1

# Un menu, para que el usario tenga claridad de lo que esta comprando
print("BIENVENIDO A 'EL BUEN GORDIDO'")
print(" < 1 > hamburguesas sencillas 2000$")
print(" < 2 > hamburguesas sencillas 2500$")
print(" < 3 > hamburguesas sencillas 3000$")
print("RECORDAR EL DESCUENTO AL PAGAR CON TARJETA!!!!")
print("")

# Las variables para definir cada procedimiento
tipo = int(input("Eliga el tipo de hamburguesas: "))
cantidad = int(input("Ingrese la cantidad de hamburguesas: "))
pago = int(input("Metodo de pago < 1 > Efectivo - < 2 > Tarjeta: "))
total = 0

# Dependiendo la opcion, la cantidad y metodo de pago, se ejecutara la funcion correspondiente
if tipo == 1:
    total = cantidad * 2000
elif tipo == 2:
    total = cantidad * 2500
else:
    total = cantidad * 3000

if pago == 1:
    print("Tiene que pagar: ",total)
else:
    descuento = total - (total*0.05)
    print("Tiene que pagar: ",descuento)
