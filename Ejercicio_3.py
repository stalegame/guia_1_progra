#Thomas Aráguiz - Progra_1

#El while cumple la funcion de dar un limite, a la cantidad de trajes
numero_trajes = 0
while numero_trajes < 4:

    numero_trajes = numero_trajes + 1
    traje = int(input("Ingrese el valor del traje que desea comprar: "))

    #El if es netamente para comprobar que tipo de descunto se le aplica a la compra
    if traje > 10000:
        descuento = traje * 0.15
        pagar = traje - descuento
    else:
        descuento = traje *0.08
        pagar = traje - descuento

    print("El descuentos es de: ",descuento)
    print("Lo que debe pagar sera de: ",pagar)
