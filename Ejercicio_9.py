#Thomas Aráguiz - Progra_1

# se piden las cordenadas correspondientes
x1 = int(input("Ingrese x1: "))
y1 = int(input("Ingrese y1: "))
x2 = int(input("Ingrese x2: "))
y2 = int(input("Ingrese y2: "))

# segun sea mayor o menor, se compara para no tener error por cuadrante en el plano
if x2 > x1:
    cateto1 = x2 - x1
else:
    cateto1 = x1 - x2

if y2 > y1:
    cateto2 = y2 - y1
else:
    cateto2 = y1 - y2

# Y usando el teorema de pitagora se consigue la distancia de los puntos
teorema = (cateto1 ** 2 + cateto2 ** 2) ** 0.5

print("La distancia es: ",teorema)
